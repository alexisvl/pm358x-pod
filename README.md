# PM358x logic analyzer pod

WARNING: I'm still waiting for fab on these boards. Everything here is
preliminary.

This is a replacement logic analyzer pod for the Philips PM358x (PM3580,
PM3585) logic analyzer series from the 1990s. They're quite capable, and often
affordable, logic analyzers for those of us who desire to fart around with wide
parallel interfaces, but they require special probe pods with an oscilloscope
probe style circuit, and the originals are few and far between.

The design, as specified here, can be built up for around $25 per pod including
all cables and connectors. That's steep, especially if you have the six-pod
unit, but all the parts are generic so feel free to cheap out on some of them.
The ones I recommend here are selected with care for reasons explained in the
schematic.

## License

This is open source hardware under the "I don't even think the system necessary
to enforce licenses should exist" license. Copyright was invented by Big
Copyright to sell more copyright. Fuckin' steal it and tell your friends you
designed it for all I care. Make it your gay crime of the day.

## PCBs

Pretty much anybody can make these boards (8/8 track and space, 1.6mm double
sided). They were originally intended to be made at JLCPCB, who will make ten
panels of eight each for about $14 total.

I probably still have some bare boards left over from my own production run,
contact me and I'll happily send over a panel of eight for the cost of the panel
plus shipping and a little bit to compensate me for my time. They're no fun to
solder with an iron (a panel has 640 surface mount pads to solder!), so if you
would like me to stencil on some paste and reflow it first so you can just put
parts on with some tacky flux and a second reflow, let me know.

If you make some on your own and have extra, I think it'd be pretty dope if you
extended this same offer to your friends. You'll definitely have spares.
